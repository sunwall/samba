/*
 * Copyright (C) 2020, Abhay Jain, Leo Fan.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

/*
 * vdfs_readdir_plus.h ---
 *
 *      Defines serialization objects for read dir plus extended attribute.
 */

#ifndef __VDFS_READ_DIR_PLUS_H__
#define __VDFS_READ_DIR_PLUS_H__

#ifndef STATIC_ASSERT
#define STATIC_ASSERT static_assert
#endif

#ifdef __cplusplus
extern "C" {
#endif

const uint8 READ_DIR_PLUS_VERSION = 1;

/*
 * Following structures are used for read dir plus extended attribute response.
 * This extended attribute returns dir entries and as well as attribute for
 * each entry.
 */

#pragma pack(push, 1)
typedef struct VDFSQid {
   uint64 path;
   uint32 version;
   uint8  type;   /* a P9QidType */
} VDFSQid;
#pragma pack(pop)


/*
 * VDFSReaddirEntryRepr contains same information as dir entry.
 */
#pragma pack(push, 1)
typedef struct VDFSReaddirEntryRepr {
   VDFSQid  qid;
   uint64   offset;
   uint8    type;
   uint16   nameLen; // Max P9_FILENAME_LEN
   char     name[]; // name does not terminate with '\0'
} VDFSReaddirEntryRepr;
#pragma pack(pop)

STATIC_ASSERT(sizeof(VDFSReaddirEntryRepr) == 24);


/*
 * VDFSObjectAttrRepr contains same information as returned by getattr call.
 */
#pragma pack(push, 1)
typedef struct VDFSObjectAttrRepr {
   uint32 mode;
   uint32 uid;
   uint32 gid;
   uint64 nlink;
   uint64 rdev;
   uint64 size;
   uint64 blksize;
   uint64 nblocks;
   uint64 atime_sec;
   uint64 atime_nsec;
   uint64 mtime_sec;
   uint64 mtime_nsec;
   uint64 ctime_sec;
   uint64 ctime_nsec;
   uint64 btime_sec;
   uint64 btime_nsec;
   uint64 gen;
   uint64 data_version;
} VDFSObjectAttrRepr;
#pragma pack(pop)

STATIC_ASSERT(sizeof(VDFSObjectAttrRepr) == 132);


/*
 * VDFSReadDirPlusRepr contains meta information for read dir plus extended
 * attribute, which include number of entries prsent in response, version info
 * and eof entries marker.
 */
#pragma pack(push, 1)
typedef struct VDFSReadDirPlusRepr {
   uint32 numEntries;
   uint8  version;
   // EOF is set if reply contains last entry in the directory.
   uint8  eof;
   // Following data member packs data for read dir plus entries. Each entry
   // is a tuple (VDFSReaddirEntryRepr, VDFSObjectAttrRepr).
   // The total entry count is determined by numEntries.
   uint8  entries[];
} VDFSReadDirPlusRepr;
#pragma pack(pop)

STATIC_ASSERT(sizeof(VDFSReadDirPlusRepr) == 6);

#ifdef __cplusplus
}
#endif

#endif // __VDFS_READ_DIR_PLUS_H__
