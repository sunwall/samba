/* VFS module for VDFS (vSAN Distributed File System)
 * Copyright 2019-2020 VMware, Inc.
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#include "includes.h"
#include <smbd/smbd.h>
#include <system/filesys.h>
#include <system/threads.h>
#include <dirent.h>
#include <sys/errno.h>
#include "smbprofile.h"

#include "lib/util/util_tdb.h"
#include "lib/tdb/include/tdb.h"
#include "librpc/gen_ndr/xattr.h"
#include "lib/replace/system/threads.h"
#include "smbd/globals.h"

#undef DBGC_CLASS
#define DBGC_CLASS DBGC_VDFS

/* prepare some definition before include vdfs_readdir_plus.h */
typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;
/* c language does not support static_assert, so disable it */
#define STATIC_ASSERT(c)

#include "vdfs_readdir_plus.h"

#define LOG(lvl, fmt, ...)  \
	DEBUG(lvl, ("[VDFS] %s[%d]:" fmt "\n", __FUNCTION__, __LINE__, \
	      ##__VA_ARGS__))

/* align with log level in ./lib/util/debug.h */
#define LOG_ERR(fmt, ...)    LOG(DBGLVL_ERR, fmt, ##__VA_ARGS__)
#define LOG_WARNING(...)     LOG(DBGLVL_WARNING, ##__VA_ARGS__)
#define LOG_NOTICE(fmt, ...) LOG(DBGLVL_NOTICE, fmt, ##__VA_ARGS__)
#define LOG_INFO(fmt, ...)   LOG(DBGLVL_INFO, fmt, ##__VA_ARGS__)
#define LOG_DEBUG(fmt, ...)  LOG(DBGLVL_DEBUG, fmt, ##__VA_ARGS__)


#define LOG_INT(v) LOG_DEBUG("%s=%lu(0x%lx)", #v, (int64_t)v, (int64_t)v)
#define LOG_STR(v) LOG_DEBUG("%s=%s", #v, (v)?(v):"null")
#define LOG_DIR(dirp)   do { \
                           LOG_INT(dirp->fd); \
                           LOG_INT(dirp->allocation);\
                           LOG_INT(dirp->size);\
                           LOG_INT(dirp->cursor);\
                           LOG_INT(dirp->filepos);\
                           LOG_INT(dirp->errcode);\
                        } while(0)
#define LOG_ENT(entp)   do { \
                           LOG_INT(entp->d_ino); \
                           LOG_INT(entp->d_off); \
                           LOG_INT(entp->d_reclen); \
                           LOG_INT(entp->d_type); \
                           LOG_STR(entp->d_name); \
                        } while(0)
#define LOG_STAT(statp) do { \
                           LOG_INT(statp->st_ex_dev); \
                           LOG_INT(statp->st_ex_ino); \
                           LOG_INT(statp->st_ex_file_id); \
                           LOG_INT(statp->st_ex_mode); \
                           LOG_INT(statp->st_ex_nlink); \
                           LOG_INT(statp->st_ex_uid); \
                           LOG_INT(statp->st_ex_gid); \
                           LOG_INT(statp->st_ex_rdev); \
                           LOG_INT(statp->st_ex_size); \
                           LOG_INT(statp->st_ex_blksize); \
                           LOG_INT(statp->st_ex_blocks); \
                           LOG_INT(statp->st_ex_flags); \
                           LOG_INT(statp->st_ex_iflags); \
                        } while(0)

#ifndef FIELD_OFFSET
#define FIELD_OFFSET(type, field) ((short)(long)&((type*)NULL)->field)
#endif
#define DEFAULT_ALLOCATION 64*1024
#define VDFS_LOCK_INIT(lock)   pthread_mutex_init(&lock, NULL)
#define VDFS_LOCK_LOCK(lock)   pthread_mutex_lock(&lock)
#define VDFS_LOCK_UNLOCK(lock) pthread_mutex_unlock(&lock)
#define VDFS_LOCK_FINI(lock)

#define SET_ERRNO(err) errno = err

#define ROOT_SHARE_NAME "vsanfs"

/*
 * The minimal time that a dirty entry can be evicted from the cache,
 * in nanoseconds
 */
#define XATTR_CACHE_GRACE_PERIOD_NSEC (5LL * 1000 * 1000 * 1000)

/*
 * Samba needn't read this xattr in theory, however Samba seems to listxattr
 * then it fetches all xattrs for an entry. That's why we also cache it.
 *
 */
#define VMWARE_CI_VOLUME_XATTR_NAME  "VMware CaseInsensitive Volume"

typedef enum wellknown_xattr_type {
	XT_UNKNOWN          = 0,
	XT_SECURITY_NTACL   = 1,
	XT_USER_DOSATTRB    = 2,
	XT_VMWARE_CI_VOLUME = 3,
} wellknown_xattr_type_t;


static bool xattr_cache_remove(struct connection_struct *conn,
                               const struct smb_filename *smb_fname,
                               const char *xattr_name,
                               struct timespec *start_time);

static inline void xattr_cache_update_counters(
        enum XATTR_CACHE_COUNTER_T xcc_counter);


static inline int xattr_cache_lock(void) {
	int ret = pthread_mutex_lock(&global_xattr_cache->lock);
#if HAVE_ROBUST_MUTEXES
	if (ret == EOWNERDEAD) {
		pthread_mutex_consistent(&global_xattr_cache->lock);
	}
#endif
	return ret;
}

static inline int xattr_cache_unlock(void) {
	return pthread_mutex_unlock(&global_xattr_cache->lock);
}


/* redefine DIR structure */
typedef struct VDFSDIR {
	int fd;                     /* File descriptor.  */
	pthread_mutex_t lock;       /* Mutex lock for this structure.  */
	size_t allocation;          /* Space allocated for the block.  */
	size_t size;                /* Total valid data in the block.  */
	size_t cursor;              /* Current cursor into the block.  */
	off_t filepos;              /* Position of next entry to read.  */
	int errcode;                /* Delayed error code.  */
	struct dirent ent;
	VDFSReadDirPlusRepr reply;
} VDFSDIR;

/* copy dirent structure from VDFSReaddirEntryRepr */
static void vdfs_copy_dirent(struct dirent *entp, VDFSReaddirEntryRepr *entry)
{
	entp->d_ino = entry->qid.path;
	entp->d_off = entry->offset;
	entp->d_reclen = entry->nameLen + FIELD_OFFSET(struct dirent, d_name);
	entp->d_type = entry->type;
	strncpy(entp->d_name, entry->name, entry->nameLen);
	entp->d_name[entry->nameLen] = '\0';
}

/* copy SMB_STRUCT_STAT structure from various sources */
static void vdfs_copy_smbstat(SMB_STRUCT_STAT *statp,
                       VDFSObjectAttrRepr *attr,
                       VDFSReaddirEntryRepr *entry,
                       vfs_handle_struct *handle)
{
	statp->st_ex_dev     = handle->conn->base_share_dev;
	statp->st_ex_ino     = entry->qid.path;
	statp->st_ex_mode    = attr->mode;
	statp->st_ex_nlink   = attr->nlink;
	statp->st_ex_uid     = attr->uid;
	statp->st_ex_gid     = attr->gid;
	statp->st_ex_rdev    = attr->rdev;
	statp->st_ex_size    = attr->size;
	statp->st_ex_atime.tv_sec   = attr->atime_sec;
	statp->st_ex_atime.tv_nsec  = attr->atime_nsec;
	statp->st_ex_mtime.tv_sec   = attr->mtime_sec;
	statp->st_ex_mtime.tv_nsec  = attr->mtime_nsec;
	statp->st_ex_ctime.tv_sec   = attr->ctime_sec;
	statp->st_ex_ctime.tv_nsec  = attr->ctime_nsec;
	statp->st_ex_btime.tv_sec   = attr->btime_sec;
	statp->st_ex_btime.tv_nsec  = attr->btime_nsec;
	statp->st_ex_blksize = attr->blksize;
	statp->st_ex_blocks  = attr->nblocks;
	statp->st_ex_flags   = 0;
	statp->st_ex_iflags  = 0;

	/* refer to smb_stat_ex_from_stat method in vfs_glusterfs.c */
	statp->st_ex_file_id = statp->st_ex_ino;
	statp->st_ex_iflags |= ST_EX_IFLAG_CALCULATED_FILE_ID;
	statp->st_ex_itime   = statp->st_ex_btime;
	statp->st_ex_iflags |= ST_EX_IFLAG_CALCULATED_ITIME;
}

/* readdirplus via xattr 'trusted.posix.readdirplus.<offset>' */
static int vdfs_readdirplus(int fd, uint64_t offset, void* value, size_t size)
{
	ssize_t bytes;
	int saved_errno;
	/* max len = leading chars + max u64 + ending '\0'
	 *         = 26 + 20 + 1
	 *         = 47
	 */
	char name[50];
	snprintf(name, sizeof(name), "trusted.posix.readdirplus.%lu", offset);

	LOG_NOTICE("Start fgetxattr %s, fd=%d, value=0x%p, size=%lu", name, fd,
	           value, size);

	/* reading xattr with prefix 'trusted.' needs superuser privilege */
	become_root();
	bytes = fgetxattr(fd, name, value, size);
	saved_errno = errno;
	unbecome_root();

	if (bytes < 0) {
		LOG_ERR("fgetxattr %s failed err=%d, offset=%lu, size=%lu,"
			" fd=%d, bytes=%ld\n",
		        name, saved_errno, offset, size, fd, bytes);
		SET_ERRNO(saved_errno);
	} else {
		LOG_NOTICE("Finish fgetxattr %s, fd=%d, bytes=%ld",
			   name, fd, bytes);
	}
	return bytes;
}

#define VDFS_READDIRPLUS_READ_AS(dirp, type) \
	(type*)vdfs_readdirplus_read(dirp, sizeof(type))

/* read data from VDFSReadDirPlusRepr in VDFSDIR */
static uint8* vdfs_readdirplus_read(VDFSDIR *dirp, int readSize)
{
	uint8* ptr;
	LOG_DIR(dirp);
	LOG_INT(readSize);

	/* check if already reach end */
	if (dirp->cursor >= dirp->size) {
		return NULL;
	}

	if (dirp->cursor + readSize > dirp->size) {
		LOG_ERR("buffer size too small, totalSize=%lu, cursor=%lu,"
			"readSize=%d\n",
		        dirp->size, dirp->cursor, readSize);
		SET_ERRNO(EFAULT);
		return NULL;
	}

	ptr = (uint8*) &dirp->reply + dirp->cursor;
	dirp->cursor += readSize;
	return ptr;
}

/* read next VDFSReaddirEntryRepr and VDFSObjectAttrRepr from VDFSDIR */
static VDFSReaddirEntryRepr *vdfs_readdirplus_read_entry(VDFSDIR *dirp,
                                                  uint32* statusp,
                                                  VDFSObjectAttrRepr **attrp)
{
	/* cursor movement should be an atomic operation, rewind it in
	 * failure path */
	uint64 saved_cursor = dirp->cursor;
	uint8 *name;
	uint32 *status;

	/* read readdirentry */
	VDFSReaddirEntryRepr *entry = VDFS_READDIRPLUS_READ_AS(
					dirp, VDFSReaddirEntryRepr);
	if (entry == NULL) {
		/* dirp->cursor not move, need not rewind */
		return NULL;
	}

	/* read object name */
	if (entry->nameLen > 255) {
		dirp->cursor = saved_cursor;
		LOG_ERR("Too large nameLen %d for inode %lu", entry->nameLen,
		        entry->qid.path);
		SET_ERRNO(EFAULT);
		return NULL;
	}
	name = vdfs_readdirplus_read(dirp, (int)entry->nameLen);
	if (name == NULL) {
		/* reach end of buffer, rewind cursor and return NULL*/
		dirp->cursor = saved_cursor;
		return NULL;
	}

	/* read status code of object attributes */
	status = VDFS_READDIRPLUS_READ_AS(dirp, uint32);
	if (status == NULL) {
		/* reach end of buffer, rewind cursor and return NULL*/
		dirp->cursor = saved_cursor;
		return NULL;
	}

	/* if status is non-zero, there is no consequent VDFSObjectAttrRepr data */
	if (*status == 0) {
		VDFSObjectAttrRepr *attr = VDFS_READDIRPLUS_READ_AS(
						dirp, VDFSObjectAttrRepr);
		if (attr == NULL) {
			dirp->cursor = saved_cursor;
			return NULL;
		}
		*attrp = attr;
	}

	*statusp = *status;
	return entry;
}

/* allocate VDFSDIR structure for opendir and fdopendir */
static VDFSDIR* vdfs_allocdir(int fd, bool close_on_error)
{
	size_t headsize = FIELD_OFFSET(VDFSDIR, reply);
	VDFSDIR *dirp = (VDFSDIR *) talloc_zero_size(NULL,
				headsize + DEFAULT_ALLOCATION);

	if (dirp == NULL) {
		if (close_on_error) {
			int saved_errno = errno;
			close(fd);
			SET_ERRNO(saved_errno);
		}
		return NULL;
	}

	dirp->fd = fd;
	VDFS_LOCK_INIT(dirp->lock);
	dirp->allocation = DEFAULT_ALLOCATION;
	dirp->size = 0;
	dirp->cursor = 0;
	dirp->filepos = 0;
	dirp->errcode = 0;

	LOG_DIR(dirp);
	return dirp;
}

/************************/
/* Directory operations */
/************************/

/* fdopendir_fn */
static DIR *vfs_vdfs_fdopendir(vfs_handle_struct *handle,
                               files_struct *fsp,
                               const char *mask,
                               uint32_t attr)
{
	return (DIR*) vdfs_allocdir(fsp->fh->fd, false);
}

/* readdir_fn */
static struct dirent *vfs_vdfs_readdir(vfs_handle_struct *handle,
                                       DIR *dir,
                                       SMB_STRUCT_STAT *sbuf)
{
	VDFSDIR *dirp = (VDFSDIR*)dir;
	VDFSReaddirEntryRepr *entry = NULL;
	uint32 attrStatus = 0;
	VDFSObjectAttrRepr *attr = NULL;
	struct dirent* entp = NULL;
	LOG_DIR(dirp);

	/* The lock mechanism will protect the consistency of VDFSDIR structure.
	 * but readdir method is still not thread safe, as the returned dirent
	 * pointer points to dirp->ent, which will be overwritten in every
	 * call of readdir
	 */
	VDFS_LOCK_LOCK(dirp->lock);
	if (dirp->cursor >= dirp->size) {
		ssize_t bytes;
		VDFSReadDirPlusRepr* reply = NULL;
		/* check if reach EOF */
		if (dirp->size > 0 && dirp->reply.eof) {
			/* Don't modify errno when reaching EOF.  */
			goto out;
		}
		bytes = vdfs_readdirplus(dirp->fd, dirp->filepos,
					 &dirp->reply, dirp->allocation);
		if (bytes < 0) {
			goto out;
		}
		dirp->size = bytes;
		dirp->cursor = 0;

		/* read VDFSReadDirPlusRepr to increase dirp->cursor */
		reply = VDFS_READDIRPLUS_READ_AS(dirp, VDFSReadDirPlusRepr);
		LOG_INT(reply->numEntries);
		LOG_INT(reply->version);
		LOG_INT(reply->eof);
	}

	/* read next entry, if there is no more entry, will return NULL
	 * directly */
	entry = vdfs_readdirplus_read_entry(dirp, &attrStatus, &attr);
	if (entry) {
		vdfs_copy_dirent(&dirp->ent, entry);

		if (sbuf) {
			SET_STAT_INVALID(*sbuf);
			/*
			 * if status is zero, try to fill SMB_STRUCT_STAT from
			 * object attrs
			 * if status is non-zero, there is no object attribute.
			 * As this is an optimization, the callers can get attr
			 * through a separate API call if they want.
			 */
			if (attrStatus == 0) {
				SMB_ASSERT(attr);
				if (!S_ISLNK(attr->mode)) {
					/*
					 * As this is an optimization, ignore
					 * if it is a symlink.
					 * Let the callers decide whether
					 * they want the link attr
					 * info or its target attr info.
					 */
					vdfs_copy_smbstat(sbuf, attr,
							  entry, handle);
				}
			}
		}
		dirp->filepos = entry->offset;
		entp = &dirp->ent;
	}

out:
	VDFS_LOCK_UNLOCK(dirp->lock);
	return entp;
}

/* closedir_fn */
static int vfs_vdfs_closedir(vfs_handle_struct *handle,
                             DIR *dir)
{
	VDFSDIR *dirp = (VDFSDIR *) dir;
	int fd;
	if (dirp == NULL) {
		SET_ERRNO(EINVAL);
		return -1;
	}

	LOG_DIR(dirp);
	fd = dirp->fd;

	VDFS_LOCK_FINI(dirp->lock);
	talloc_destroy(dirp);
	return close(fd);
}

/* seekdir_fn */
static void vfs_vdfs_seekdir(vfs_handle_struct *handle,
                             DIR *dir,
                             long offset)
{
	VDFSDIR* dirp = (VDFSDIR*) dir;

	VDFS_LOCK_LOCK(dirp->lock);
	dirp->size = 0;
	dirp->cursor = 0;
	dirp->filepos = offset;
	VDFS_LOCK_UNLOCK(dirp->lock);

	LOG_DIR(dirp);
}

/* rewind_dir_fn */
static void vfs_vdfs_rewinddir(vfs_handle_struct *handle,
                               DIR *dir)
{
	VDFSDIR* dirp = (VDFSDIR*) dir;

	VDFS_LOCK_LOCK(dirp->lock);
	dirp->size = 0;
	dirp->cursor = 0;
	dirp->filepos = 0;
	dirp->errcode = 0;
	VDFS_LOCK_UNLOCK(dirp->lock);

	LOG_DIR(dirp);
}

/* telldir_fn */
static long vfs_vdfs_telldir(vfs_handle_struct *handle,
                             DIR *dir)
{
	VDFSDIR* dirp = (VDFSDIR*) dir;
	return dirp->filepos;
}

struct share_root_info_t {
	char *base_name; /* The connect path for the share root */
	DIR *dirp;       /* The opendir to the share root */
};


static int vfs_vdfs_chdir(vfs_handle_struct *handle,
                          const struct smb_filename *smb_fname)
{
	int ret;
	const struct share_root_info_t *share_root_info =
					handle->conn->vfs_data;

	LOG_DEBUG("%s: smb_fname: %s", __func__, smb_fname->base_name);

	/*
	 * Change dir to the tree root, use the the opendir from conn directly
	 * This avoids calling into VDFS layer for the chdir call.
	 * In PR 2564572, we discovered Samba calls into SMB_VFS_CHDIR for
	 * every write operation when the same client accesses multiple shares
	 * simultaneously.
	 */
	if (share_root_info &&
	    strcmp(share_root_info->base_name, smb_fname->base_name) == 0) {
		int fd = dirfd(share_root_info->dirp);
		START_PROFILE(syscall_fchdir);
		ret = fchdir(fd);
		END_PROFILE(syscall_fchdir);
	} else {
		ret = SMB_VFS_NEXT_CHDIR(handle, smb_fname);
	}
	return ret;
}


/* File operations */

static inline wellknown_xattr_type_t xattr_cache_get_type(
	const char *xattr_name)
{
	if (strcmp(xattr_name, XATTR_NTACL_NAME) == 0) {
		return XT_SECURITY_NTACL;
	} else if (strcmp(xattr_name, SAMBA_XATTR_DOS_ATTRIB) == 0) {
		return XT_USER_DOSATTRB;
	} else if (strcmp(xattr_name, VMWARE_CI_VOLUME_XATTR_NAME) == 0) {
		return XT_VMWARE_CI_VOLUME;
	}
	return XT_UNKNOWN;
}


/*
 * Generate xattr key for a given path and xattr name.
 * We only generate xattr key for known xattrs.
 *
 * @return whether the key was generated successfully
 * return false for unknown xattrs
 * return false if the key_buf isn't large enough for the key
 * return true on other conditions
 */

static bool xattr_cache_generate_key(const struct connection_struct *conn,
				     const struct smb_filename *smb_fname,
				     const char *xattr_name, char *key_buf,
				     const size_t key_buf_len)
{

	ino_t st_ex_ino = smb_fname->st.st_ex_ino;
	ssize_t actual_len;
	wellknown_xattr_type_t type = xattr_cache_get_type(xattr_name);

	assert(key_buf_len > 0);

	/*
	 * We only cache known xattrs.
	 * Now we only cache xattrs used for implementing Windows ACLs.
	 */
	if (type == XT_UNKNOWN) {
		return false;
	}

	if (st_ex_ino == 0) {
		struct stat statbuf;
		int ret;
		START_PROFILE(syscall_stat);
		ret = stat(smb_fname->base_name, &statbuf);
		END_PROFILE(syscall_stat);

		if (ret == 0) {
			st_ex_ino = statbuf.st_ino;
			assert(st_ex_ino != 0);
			LOG_INFO("Got ino %016lX for %s",
				 st_ex_ino, smb_fname->base_name);
		} else {
			return false;
		}
	}

	/* The key looks like this:
	 * 1:09732960673f8409:/vsfs/06eae65e-8370-20e4-ca84-c81f66df5eee/volumes/35236812-bc07-1127-1e79-b21013179229/default
	 */
	actual_len = snprintf(key_buf, key_buf_len, "%1d:%016lX:%95s",
			      type, st_ex_ino, conn->connectpath);

	/*
	 * If the xattr key is too long, we won't cache it
	 */
	return actual_len > 0 && actual_len < key_buf_len;
}


/*
 * The global_xattr_cache->lock should have been acquired by the caller
 */
static struct xattr_cache_entry *xattr_hash_lookup(
		const char *xattr_key,
		unsigned int hash_value)
{
	struct xattr_cache_entry *hash_bucket =
	        global_xattr_cache->hashtable[hash_value
	                % global_xattr_cache->num_entries];

	while (hash_bucket) {
		if (strncmp(hash_bucket->key, xattr_key,
			    XATTR_CACHE_KEY_SIZE) == 0) {
			return hash_bucket;
		}
		hash_bucket = hash_bucket->next;
	}
	return NULL;
}


/*
 * The global_xattr_cache->lock should have been acquired by the caller
 */
static void xattr_hash_insert(
		unsigned int hash_value, struct xattr_cache_entry *entry)
{
	struct xattr_cache_entry **hash_bucket_head =
	        &global_xattr_cache->hashtable[hash_value
	                % global_xattr_cache->num_entries];

	entry->next = *hash_bucket_head;
	*hash_bucket_head = entry;
	entry->in_hash = true;
}

/*
 * The global_xattr_cache->lock should have been acquired by the caller
 */
static bool xattr_hash_remove(const char *xattr_key)
{
	TDB_DATA key = string_tdb_data(xattr_key);
	unsigned int hash_value = tdb_jenkins_hash(&key);

	struct xattr_cache_entry **hash_bucketp =
	        &global_xattr_cache->hashtable[hash_value
	                % global_xattr_cache->num_entries];

	while (*hash_bucketp) {
		struct xattr_cache_entry *hash_bucket = *hash_bucketp;
		if (strncmp(hash_bucket->key, xattr_key,
			    XATTR_CACHE_KEY_SIZE) == 0) {
			*hash_bucketp = hash_bucket->next;
			hash_bucket->next = NULL;
			hash_bucket->in_hash = false;
			LOG_DEBUG("Removed xattr %s from hashtable ",
				  xattr_key);
			return true;
		}
		hash_bucketp = &hash_bucket->next;
	}

	return false;
}


/*
 * actual_size = 0 is a valid output, which means xattr doesn't exist.
 * Return false, if a xattr can't be found from cache.
 */
static bool xattr_cache_lookup(struct connection_struct *conn, /* In */
                               const struct smb_filename *smb_fname, /* In */
                               const char *xattr_name, /* In */
                               void *value,        /* In/Out */
                               size_t size,        /* In */
                               ssize_t *actual_size /* Out */)
{
	char xattr_key_buf[XATTR_CACHE_KEY_SIZE];
	bool ret = false;
	TDB_DATA key;
	unsigned int hash_value;
	struct xattr_cache_entry *entry;

	if (!global_xattr_cache) {
		return false;
	}

	if (!xattr_cache_generate_key(conn, smb_fname, xattr_name,
	                              xattr_key_buf, XATTR_CACHE_KEY_SIZE)) {
		return false;
	}

	key = string_tdb_data(xattr_key_buf);
	hash_value = tdb_jenkins_hash(&key);

	xattr_cache_lock();

	entry = xattr_hash_lookup(xattr_key_buf, hash_value);

	if (entry && !entry->dirty) {
		LOG_DEBUG("Found from cache for %s (name: %s)," "hash_value=%u,"
				  "hash_bucket=%p, xattr size=%d, dirty=%s",
				  xattr_key_buf, smb_fname->base_name,
				  hash_value, entry, entry->value_size,
				  entry->dirty ? "true" : "false");
		if (entry->value_size <= size) {
			memcpy(value, entry->value, entry->value_size);
		}
		entry->ref = true;
		*actual_size = entry->value_size;
		ret = true;
	}

	xattr_cache_unlock();

	if (ret) {
		xattr_cache_update_counters(XCC_HIT);
	} else {
		LOG_DEBUG("Cache missed for %s (name: %s)", xattr_key_buf,
		          smb_fname->base_name);
		xattr_cache_update_counters(XCC_MISS);
	}

	return ret;
}


/*
 * Whether an entry can be evicted
 */
static bool xattr_cache_can_evict(struct xattr_cache_entry *entry,
                                  struct timespec *cur_time)
{
	if (entry->ref) {
		return false;
	}

	/* The entry is empty */
	if (timespec_is_zero(&entry->update_time)) {
		return true;
	}

	if (nsec_time_diff(cur_time, &entry->update_time) < XATTR_CACHE_GRACE_PERIOD_NSEC) {
		return false;
	}

	return true;
}

static void xattr_cache_entry_mark_dirty(struct xattr_cache_entry *entry,
                                 struct timespec *cur_time)
{
	entry->dirty = true;
	entry->update_time = *cur_time;
}

static void xattr_cache_entry_update(struct xattr_cache_entry *entry,
                              const char *xattr_key, const char *value,
                              ssize_t size, struct timespec *cur_time)
{
	/* Caller can pass NULL if the xattr_key is the same as that in
	 * the entry */
	if (xattr_key) {
		strncpy(entry->key, xattr_key, sizeof(entry->key));
	}
	if (size > 0 && size <= XATTR_CACHE_VALUE_SIZE) {
		memcpy(entry->value, value, size);
	}
	entry->ref = true;
	entry->value_size = size;
	entry->update_time = *cur_time;
	entry->dirty = false;
}

/*
 * @param start_time  The time when the disk operation starts
 *
 * Update an existing xattr cache entry or add a new entry
 *
 * if start_time > time stamp in the cache entry, update the cache entry
 * else remove the cache entry.
 * When start_time <= current_time, it indicates there is some race, we're not
 * sure which version should we use, so just throw away the cache entry.
 *
 * @return true if update or add succeeded
 *         false if the entry is removed from the cache
 */

static bool xattr_cache_update(struct connection_struct *conn,
                               const struct smb_filename *smb_fname,
                               const char *xattr_name, const void *value,
                               ssize_t size,
                               struct timespec *start_time)
{
	struct xattr_cache_entry *entry = NULL;
	char xattr_key_buf[XATTR_CACHE_KEY_SIZE];
	bool ret = false;
	unsigned int hash_value;
	struct timespec cur_time;
	int i;
	TDB_DATA key;

	if (!global_xattr_cache) {
		return false;
	}

	if (!xattr_cache_generate_key(conn, smb_fname, xattr_name,
	                              xattr_key_buf, XATTR_CACHE_KEY_SIZE)) {
		return false;
	}

	key = string_tdb_data(xattr_key_buf);
	hash_value = tdb_jenkins_hash(&key);

	xattr_cache_lock();

	clock_gettime_mono(&cur_time);

	/* Check whether the xattr exists in the cache */
	entry = xattr_hash_lookup(xattr_key_buf, hash_value);

	if (entry) {
		/*
		 * When start time is earlier than the entry update time, it implies
		 * multiple clients tries to update the xattr simultaneously. We're not
		 * sure the xattr value on disk, so we just mark the cache as dirty
		 * so that next read will re-populate the cache. This resolves ABBA
		 * issue with a lock-less way.
		 */
		if (size > XATTR_CACHE_VALUE_SIZE ||
			timespec_compare(start_time,
					 &entry->update_time) <= 0) {
			LOG_NOTICE("Mark xattr %s (name:%s, size=%ld) as dirty ",
			xattr_key_buf, smb_fname->base_name, size);
			xattr_cache_entry_mark_dirty(entry, &cur_time);
		} else {
			xattr_cache_entry_update(entry, NULL, value,
						 size, &cur_time);
			ret = true;
		}
		goto out;
	}

	/* Find an available slot */
	for (i = global_xattr_cache->clock_hand;
	     i < global_xattr_cache->num_entries * 2 + \
		 global_xattr_cache->clock_hand; i++) {
		int index = i % global_xattr_cache->num_entries;
		struct xattr_cache_entry *ent =
			&global_xattr_cache->entries[index];
		if (xattr_cache_can_evict(ent, &cur_time)) {
			/* Remove this entry from hash table */
			if (ent->in_hash) {
				xattr_hash_remove(ent->key);
			}
			entry = ent;
			xattr_cache_entry_update(entry, xattr_key_buf,
						 value, size, &cur_time);
			xattr_hash_insert(hash_value, entry);
			global_xattr_cache->clock_hand = index + 1;
			ret = true;
			break;
		} else {
			global_xattr_cache->entries[index].ref = false;
		}
	}

out:
	if (entry == NULL) {
		/*
		 * Now we only evict entries which are at least
		 * XATTR_CACHE_GRACE_PERIOD ns old. So in theory it's possible
		 * we can't find any entry for eviction.
		 */
		LOG_WARNING("Xattt cache is full!! Failed to find an cache "
			    "entry for %s (name:%s, size=%ld)",
			    xattr_key_buf, smb_fname->base_name, size);
	} else {
		LOG_INFO("Updated xattr cache for %s (name:%s, size=%ld) "
				 "hash_value=%u entry=%p, clock_hand=%lu",
				 xattr_key_buf, smb_fname->base_name, size,
				 hash_value, entry,
				 global_xattr_cache->clock_hand);
	}

	xattr_cache_unlock();
	xattr_cache_update_counters(XCC_UPDATE);
	return ret;
}


static bool xattr_cache_remove(struct connection_struct *conn,
                               const struct smb_filename *smb_fname,
                               const char *xattr_name,
                               struct timespec *start_time)
{
	struct xattr_cache_entry *entry = NULL;
	char xattr_key_buf[XATTR_CACHE_KEY_SIZE];
	bool removed = false;
	TDB_DATA key;
	unsigned int hash_value;

	if (!global_xattr_cache) {
		return false;
	}

	if (!xattr_cache_generate_key(conn, smb_fname, xattr_name,
	                              xattr_key_buf, XATTR_CACHE_KEY_SIZE)) {
		return false;
	}

	key = string_tdb_data(xattr_key_buf);
	hash_value = tdb_jenkins_hash(&key);

	xattr_cache_lock();
	entry = xattr_hash_lookup(xattr_key_buf, hash_value);

	if (entry) {
		struct timespec cur_time;
		clock_gettime_mono(&cur_time);
		xattr_cache_entry_mark_dirty(entry, &cur_time);
		xattr_cache_update_counters(XCC_UPDATE);
		LOG_DEBUG("Marked %s (name:%s) as dirty in xattr cache",
		          xattr_key_buf, smb_fname->base_name);
		removed = true;
	}

	xattr_cache_unlock();

	return removed;
}


/*
 * The counters are statistics which are used for calculating cache hit ratio.
 * No need to ensure its preciseness, so no lock is needed.
 */
static inline void xattr_cache_update_counters(
        enum XATTR_CACHE_COUNTER_T xcc_counter)
{
	if (global_xattr_cache) {
		global_xattr_cache->cache_counters[xcc_counter]++;
	}
}


static inline void xattr_cache_print_counters(void)
{
	if (global_xattr_cache) {
		LOG_NOTICE(
		        "global xattr cache hits:%lu, misses: %lu, update: %lu",
		        global_xattr_cache->cache_counters[XCC_HIT],
		        global_xattr_cache->cache_counters[XCC_MISS],
		        global_xattr_cache->cache_counters[XCC_UPDATE]);
	}
}


/****************************************************************
 Extended attribute operations.
*****************************************************************/

static ssize_t vfs_vdfs_getxattr(struct vfs_handle_struct *handle,
			const struct smb_filename *smb_fname,
			const char *name,
			void *value,
			size_t size)
{
	ssize_t actual_size = 0;

	bool cache_hit = xattr_cache_lookup(handle->conn, smb_fname, name,
					    value, size, &actual_size);
	if (!cache_hit) {
		struct timespec start_time;
		clock_gettime_mono(&start_time);
		actual_size = SMB_VFS_NEXT_GETXATTR(handle, smb_fname, name,
						    value, size);
		xattr_cache_update(handle->conn, smb_fname, name, value,
				   actual_size, &start_time);
	}

	return actual_size;
}


static ssize_t vfs_vdfs_fgetxattr(struct vfs_handle_struct *handle,
                                  struct files_struct *fsp,
                                  const char *name, void *value, size_t size)
{
	ssize_t actual_size = 0;

	bool cache_hit = xattr_cache_lookup(handle->conn, fsp->fsp_name,
	                                   name, value, size, &actual_size);
	if (!cache_hit) {
		struct timespec start_time;
		clock_gettime_mono(&start_time);
		actual_size = SMB_VFS_NEXT_FGETXATTR(handle, fsp, name, value, size);
		xattr_cache_update(handle->conn, fsp->fsp_name, name, value,
		                   actual_size, &start_time);
	}

	return actual_size;
}


static int vfs_vdfs_removexattr(struct vfs_handle_struct *handle,
				const struct smb_filename *smb_fname,
				const char *name)
{
	int ret;
	struct timespec start_time;
	clock_gettime_mono(&start_time);
	ret = SMB_VFS_NEXT_REMOVEXATTR(handle, smb_fname, name);
	if (ret == 0) {
		xattr_cache_remove(handle->conn, smb_fname, name, &start_time);
	}
	return ret;
}

static int vfs_vdfs_fremovexattr(struct vfs_handle_struct *handle,
                                 struct files_struct *fsp, const char *name)
{
	int ret;
	struct timespec start_time;
	clock_gettime_mono(&start_time);
	ret = SMB_VFS_NEXT_FREMOVEXATTR(handle, fsp, name);
	if (ret == 0) {
		xattr_cache_remove(handle->conn, fsp->fsp_name, name,
				   &start_time);
	}
	return ret;
}


static int vfs_vdfs_setxattr(struct vfs_handle_struct *handle,
				const struct smb_filename *smb_fname,
				const char *name,
				const void *value,
				size_t size,
				int flags)
{
	int ret;
	struct timespec start_time;
	clock_gettime_mono(&start_time);
	ret = SMB_VFS_NEXT_SETXATTR(handle, smb_fname, name, value, size,
				   flags);
	if (ret == 0) {
		xattr_cache_update(handle->conn, smb_fname, name, value, size,
		                   &start_time);
	}
	return ret;
}


static int vfs_vdfs_fsetxattr(struct vfs_handle_struct *handle,
                              struct files_struct *fsp,
                              const char *name, const void *value,
                              size_t size, int flags)
{
	int ret;
	struct timespec start_time;
	clock_gettime_mono(&start_time);
	ret = SMB_VFS_NEXT_FSETXATTR(handle, fsp, name, value, size, flags);
	if (ret == 0) {
		xattr_cache_update(handle->conn, fsp->fsp_name, name, value, size,
		                   &start_time);
	}

	return 0;
}


/*******************/
/* Disk operations */
/*******************/
static int vfs_vdfs_statvfs(struct vfs_handle_struct *handle,
                            const struct smb_filename *smb_fname,
                            vfs_statvfs_struct *statbuf)
{
	int ret = SMB_VFS_NEXT_STATVFS(handle, smb_fname, statbuf);
	/* Indicate VDFS supports case insensitive lookup */
	statbuf->FsCapabilities &= ~FILE_CASE_SENSITIVE_SEARCH;
	return ret;
}


static int vfs_vdfs_connect(struct vfs_handle_struct *handle,
                            const char *service, const char *user)
{
	LOG_NOTICE("%s: user %s connected to service %s connectpath: %s "
		   "(pid %d)\n",
	         __func__, user, service, handle->conn->connectpath,
	         (int )getpid());

	/*
	 * A lot of clients may connect to root share vsanfs, and it just asks
	 * for the referral without many other operations. Caching opendir for
	 * the vsanfs would consume more memory without adding any benefit.
	 */
	if (strcmp(service, ROOT_SHARE_NAME) != 0) {
		struct share_root_info_t *share_root_info = talloc_zero(
		        NULL, struct share_root_info_t);
		if (share_root_info) {
			LOG_NOTICE("%s: Connect to : %s", __func__,
			         handle->conn->connectpath);
			share_root_info->dirp = opendir(
				handle->conn->connectpath);
			if (share_root_info->dirp != NULL) {
				share_root_info->base_name = talloc_strdup(
				        NULL, handle->conn->connectpath);
			}
		}

		assert(handle->conn->vfs_data == NULL);
		handle->conn->vfs_data = share_root_info;
	}

	handle->conn->cwd_not_modified = true;

	return 0;
}

static void vfs_vdfs_disconnect(struct vfs_handle_struct *handle)
{
	struct share_root_info_t *share_root_info = handle->conn->vfs_data;

	LOG_NOTICE("%s: handle %p, Remove opendir from cache: %s (pid %d)\n",
	         __func__, handle, handle->conn->connectpath, (int )getpid());

	if (share_root_info && share_root_info->dirp) {
		xattr_cache_print_counters();
		closedir(share_root_info->dirp);
		talloc_destroy(share_root_info->base_name);
	}

	talloc_destroy(share_root_info);
	handle->conn->vfs_data = NULL;
}


static struct vfs_fn_pointers vfs_vdfs_fns = {
	/* Disk operations */

	.statvfs_fn    = vfs_vdfs_statvfs,
	.connect_fn    = vfs_vdfs_connect,
	.disconnect_fn = vfs_vdfs_disconnect,

	/* Directory operations */
	.fdopendir_fn  = vfs_vdfs_fdopendir,
	.readdir_fn    = vfs_vdfs_readdir,
	.closedir_fn   = vfs_vdfs_closedir,
	.seekdir_fn    = vfs_vdfs_seekdir,
	.rewind_dir_fn = vfs_vdfs_rewinddir,
	.telldir_fn    = vfs_vdfs_telldir,
	.chdir_fn      = vfs_vdfs_chdir,

	/* EA operations. */
	.getxattr_fn     = vfs_vdfs_getxattr,
	.fgetxattr_fn    = vfs_vdfs_fgetxattr,
	.removexattr_fn  = vfs_vdfs_removexattr,
	.fremovexattr_fn = vfs_vdfs_fremovexattr,
	.setxattr_fn     = vfs_vdfs_setxattr,
	.fsetxattr_fn    = vfs_vdfs_fsetxattr,
};


static_decl_vfs;
NTSTATUS vfs_vdfs_init(TALLOC_CTX *ctx)
{
	NTSTATUS ret;

	ret = smb_register_vfs(SMB_VFS_INTERFACE_VERSION, "vdfs",
	                       &vfs_vdfs_fns);
	return ret;
}
